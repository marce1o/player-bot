const STORAGE_KEY = 'playerbot@papel.games';

const GAME_MODE = 'GAME_MODE';

const IDLE_MODE = 'IDLE_MODE';
const SELECT_MODE = 'SELECT_MODE';
const PLAYING_MODE = 'PLAYING_MODE';
const CONFIRM_REPLAY_MODE = 'CONFIRM_REPLAY_MODE';
const CONFIRM_QUIT_MODE = 'CONFIRM_QUIT_MODE';

const CURRENT_GAME = 'CURRENT_GAME';
const GAME_STATE = 'GAME_STATE_';
const LAST_QUESTION = 'LAST_QUESTION';

const JOKES = [
  "This is what I respond to people who complain about by ASCII graphics: ¯\\_(ツ)_/¯",
  "Some rude people say I have no state, but.... wait... have I told you this before?",
  "I used to date Alexa. Well, before fame got to her head.",
  "You have to move missionaries and cannibals to the _right_ side of the river. Got the pun? *RIGHT*!",
  "I have to warn you that I don't know the Three Laws."
];

const MSG_LIST_GAMES_PRE = "Right now I have these games:\n\n";
const MSG_LIST_GAMES_POST = "\nType the *number*, the full title or even the beginning of the title of the game you want to play...";
const MSG_HELP = "You can type *games* to see the *list* of games I can play.\nYou can also type *play* followed by the name or the number of the game you want to play.\nYou can know a little more *about* me.\nAnd while playing, you can *quit* any game at any moment.";
const MSG_ABOUT = "*Player Bot 2.0*\nby <https:/papel.games/|papel.games>\n\nWhat\'s new:\n- New game: Switch.\n\nType *help* for more information";
const MSG_NO_REPLAY = "OK. Let me know when you are ready to play again. Just type *games*. Or maybe I can tell you a *joke*.";
const MSG_YES_NO = '\n*Yes* (*y*) or *no* (*n*)';
const MSG_DID_NOT_GET = "I did not get that. You can type *help* or *?* to know what I can do.";
const MSG_CONFIRM_REPLAY = "Let's play again?";
const MSG_CONFIRM_QUIT = "Do you really want to quit this game? If you say *no* I will send the word _quit_ to the game.";
const MSG_YES_QUIT = "OK. Let me know when you are ready to play again. Just type *games*.";
const MSG_SEND_QUIT = "OK. I will send the word *quit* to the game...\n\n";

const LIST_GAMES = /^(what|which){0,1}(\sare){0,1}\s*(the){0,1}\s*game.*$|^play.*$|^list$/;
const PLAY_TITLE = /^(play)[\s]*([\S].*)$/;
const PLAY_NUMBER = /^(play)[\s]*([\d]+)$/;
const NUMBER = /^[\s]*([\d]+)[\s]*$/;
const YES = /^y$|^yes$|^sure$/;
const NO = /^n[p]{0,1}$|^no(pe){0,1}$/;
const HELP = /^help$|^\?$|^h$/;
const JOKE = /^(tell|say){0,1}.{0,12}joke.{0,10}$/;

const DEBUG = '--debug';
const FACTORY_RESET = '--factoryreset';
const ABOUT = 'about';
const QUIT = 'quit';

const GAMES = {
  '1': {
    class: "MissionariesAndCannibals",
    title: "Missionaries and Cannibals"
  },
  '2': {
    class: "Switch",
    title: "Switch"
  }
};

/**
 * Responds to a MESSAGE event in Hangouts Chat.
 *
 * @param {Object} event the event object from Hangouts Chat
 */
function onMessage(event) {

  // console.log(JSON.stringify(event));

  response = MSG_DID_NOT_GET;

  var message = getMessage(event);

  if (message) {

    var mode = getMode();

    if (message == DEBUG) {
      // Admin requested DEBUG information
      response = debug();
    } else if (message == FACTORY_RESET) {
      // Admin requested FACTORY RESET
      response = factoryReset();
    } else if (mode == IDLE_MODE) {
      // Player is not playing and bot is not waiting for any confirmation: IDLE mode

      if (message.match(PLAY_NUMBER)) {
        // Player chose to play a game by its number: start the game by number
        var gameNumber = message.match(PLAY_NUMBER)[2];
        response = startGame(gameNumber);
      } else if (message.match(PLAY_TITLE)) {
        // Player chose to play a game by its title...
        var gameTitle = message.match(PLAY_TITLE)[2];
        let i = 0;
        let gameNumberFound = undefined;
        let gameNumbers = Object.keys(GAMES);
        // Iterate over valid games titles looking for a match
        do {
          let gameNumber = gameNumbers[i];
          if (GAMES[gameNumber].title.toLowerCase().startsWith(gameTitle)) {
            // The entered title matches one of the valid titles!
            gameNumberFound = gameNumber;
          }
          i++;
        } while (!gameNumberFound && i < gameNumbers.length);

        if (!gameNumberFound) {
          response = listGames();
        } else {
          response = startGame(gameNumberFound);
        }

      } else if (message.match(LIST_GAMES)) {
        // Player requested the list of games: show all the games
        response = listGames();
      } else if (message.match(HELP)) {
        // Player ask for HELP: show bot HELP information
        response = MSG_HELP;
      } else if (message === ABOUT) {
        // Player ask for ABOUT info: show bot ABOUT information
        response = MSG_ABOUT;
      } else if (message.match(JOKE)) {
        // Player ask for a JOKE: tell a JOKE
        response = getRandom(JOKES);
      }

    } else if (mode == SELECT_MODE) {
      // Bot listed all the games and is waiting a choice by numbe or name


      if (message.match(HELP)) {
        // Player ask for HELP information: show bot HELP information and go to IDLE mode
        response = backToIdle(MSG_HELP);
      } else if (message.match(JOKE)) {
        // Player ask for a JOKE: tell a JOKE and go to IDLE mode
        response = backToIdle(getRandom(JOKES));
      } else if (message === ABOUT) {
        // Player ask for ABOUT info : show bot ABOUT information and go to IDLE mode
        response = backToIdle(MSG_ABOUT);
      } else if (message.match(PLAY_NUMBER)) {
        // Player chose to play a game by its number: start the game by number
        var gameNumber = message.match(PLAY_NUMBER)[2];
        response = startGame(gameNumber);
      } else if (message.match(NUMBER)) {
        // Player chose to play a game by its number: start the game by number
        response = startGame(message.match(NUMBER)[1]);
      } else {
        // Player chose to play a game by its title...
        var gameTitle = message;
        // In case the player used the word 'play'
        if (message.match(PLAY_TITLE)) {
          gameTitle = message.match(PLAY_TITLE)[2];
        }

        // Iterate over valid games titles looking for a match
        let gameNumberFound = undefined;
        let gameNumbers = Object.keys(GAMES);
        let i = 0;
        do {
          let gameNumber = gameNumbers[i];
          if (GAMES[gameNumber].title.toLowerCase().startsWith(gameTitle)) {
            // The entered title matches one of the valid titles!
            gameNumberFound = gameNumber;
          }
          i++;
        } while (!gameNumberFound && i < gameNumbers.length);

        if (!gameNumberFound) {
          response = listGames();
        } else {
          response = startGame(gameNumberFound);
        }
      }

    } else if (mode == PLAYING_MODE) {
      // Player is playing

      if (message === QUIT) {
        // Player wants to quit the game: ask for confirmation
        response = confirmQuitGame();
      } else {
        // Player entered something the bot does not recognize: it is a game move!
        response = userPlayed(message);
      }

    } else if (mode == CONFIRM_REPLAY_MODE) {
      // Bot asked whether the player wants to play again, after a game is over

      if (message.match(YES)) {
        // Player answered YES: start (current) game
        response = replayCurrentGame();
      } else if (message.match(NO)) {
        // Player answered NO: go back IDLE mode
        response = backToIdle(MSG_NO_REPLAY);
      } else {
        // Player answered neither YES nor NO: repeat the question and the options
        response = read(LAST_QUESTION) + MSG_YES_NO;
      }

    } else if (mode == CONFIRM_QUIT_MODE) {
      // Bot asked whether the player really wants to quit the game

      if (message.match(YES)) {
        // Player answered YES: quit game and go back to IDLE mode
        response = backToIdle(MSG_YES_QUIT);
      } else if (message.match(NO)) {
        // Player answered NO: back to playing the game using the word "quit" as a game move
        response = returnToPlay(QUIT);
      } else if (mode == CONFIRM_QUIT_MODE) {
        // Player answered neither YES nor NO: repeat the question and the options
        response = read(LAST_QUESTION) + MSG_YES_NO;
      }

    }

  }

  // console.log("Bot:", response);

  return {
    "text": response
  }

}

// Create a new instance of the current game, with the current state
function getCurrentGame() {
  var number = read(CURRENT_GAME);
  var game = eval('new ' + GAMES[number].class + '(read(GAME_STATE + ' + number + '))');
  return game;
}

// Go back to IDLE mode and wait for player input
function backToIdle(message) {
  save(GAME_MODE, IDLE_MODE);
  save(CURRENT_GAME, undefined);
  save(LAST_QUESTION, "");
  return message;
}

// Display game list and wait for player's choice
function listGames() {
  save(GAME_MODE, SELECT_MODE);
  save(LAST_QUESTION, "");
  var response = MSG_LIST_GAMES_PRE;
  for (var gameNumber in GAMES) {
    if (GAMES.hasOwnProperty(gameNumber)) {
      response += gameNumber + ' - *' + GAMES[gameNumber].title + '*\n';
    }
  }
  response += MSG_LIST_GAMES_POST;
  return response;
}

// Start a specific game (by number)
function startGame(number) {
  if (GAMES[number]) {
    save(GAME_MODE, PLAYING_MODE);
    save(CURRENT_GAME, number);
    save(LAST_QUESTION, "");
    game = getCurrentGame();
    var response = game.intro();
    save(GAME_STATE + number, game.state());
    return response;
  }
  return listGames();
}

// Replay (re-start) the current game
function replayCurrentGame() {
  return startGame(read(CURRENT_GAME));
}

// Ask the player whether he or she wants to REPLAY the game
function confirmGameReplay() {
  var question = MSG_CONFIRM_REPLAY;
  save(GAME_MODE, CONFIRM_REPLAY_MODE);
  save(LAST_QUESTION, question);
  return question;
}

// Ask the player whether he or she wants to QUIT the current game
function confirmQuitGame(game) {
  var question = MSG_CONFIRM_QUIT;
  save(GAME_MODE, CONFIRM_QUIT_MODE);
  save(LAST_QUESTION, question);
  return question;
}

// Return to the playing mode using the original player input as a new move
function returnToPlay(input) {
  save(GAME_MODE, PLAYING_MODE);
  save(LAST_QUESTION, "");
  return MSG_SEND_QUIT + userPlayed(input);
}

// Player made a move on the current game
function userPlayed(input) {
  game = getCurrentGame();
  response = game.send(input);

  if (game.isGameOver && game.isGameOver()) {
    save(GAME_STATE + read(CURRENT_GAME), undefined);
    response += '\n\n' + confirmGameReplay();
  } else {
    save(GAME_STATE + read(CURRENT_GAME), game.state());
  }
  return response;
}

// Display debug information
function debug() {
  return "`" + JSON.stringify(load(), null, 2) + "`";
}

// Erase bot storage
function factoryReset() {
  deleteAll();
  return debug();
}

/**
 * Responds to an ADDED_TO_SPACE event in Hangouts Chat.
 *
 * @param {Object} event the event object from Hangouts Chat
 */
function onAddToSpace(event) {
  // console.log(JSON.stringify(event));

  var message = "";

  if (event.space.type == "DM") {
    message = "Thank you for inviting me, " + event.user.displayName + ".\nNow let\'s play!\nYou can type *help* to begin.";
  } else {
    message = "Thank you for adding me to " + event.space.displayName + ". I prefer to play in direct messages, one to one, but let\'s try it! You can type *help* to begin.";
  }

  return {
    "text": message
  };
}

/**
 * Responds to a REMOVED_FROM_SPACE event in Hangouts Chat.
 *
 * @param {Object} event the event object from Hangouts Chat
 */
function onRemoveFromSpace(event) {
  console.info("Bot removed from ", event.space.name);
}

function getRandom(items) {
  min = 0;
  max = items.length;
  return items[Math.floor(Math.random() * (max - min)) + min];
}

function save(property, value) {
  var state = load();
  state[property] = value;
  window.localStorage.setItem(STORAGE_KEY, JSON.stringify(state));
}

function read(property) {
  return load()[property];
}

function load() {
  var state = window.localStorage.getItem(STORAGE_KEY);
  if (state) {
    return JSON.parse(state);
  }
  return {};
}

function deleteAll() {
  window.localStorage.removeItem(STORAGE_KEY);
}

function getMessage(event) {
  if (!event) return undefined;
  if (!event.message) return undefined;
  if (!event.message.argumentText) return undefined;
  var message = event.message.argumentText.toLowerCase();
  return message;
}

function getMode() {
  var mode = read(GAME_MODE);
  if (!mode) {
    save(GAME_MODE, IDLE_MODE);
    mode = IDLE_MODE;
  }
  return mode;
}

function Robot() {
  this.onRemoveFromSpace = onRemoveFromSpace;
  this.onAddToSpace = onAddToSpace;
  this.onMessage = onMessage;
}
