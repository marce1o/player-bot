Player Bot


                         Play
      +-------------------------------------------+   +----------------------+
      |                                           |   |                      | Yes
      |                                           v   v                      |
+-----+----+            +---------+            +--+---+--+              +----+-----+
|          |    List    |         |    Play    |         |   Game o^er  |          |
|   IDLE   +----------->+ SELECT  +----------->+ PLAYING +------------->+  REPLAY  |
|          |            |         |            |         |              |          |
+--+----+--+            +---------+            +--+---+--+              +----+-----+
   ^    ^                                         |   ^                      |
   |    |                                    Quit |   | No                   | No
   |    |                                         v   |                      |
   |    |                                      +--+---+--+                   |
   |    |                 Yes                  |         |                   |
   |    +--------------------------------------+  QUIT   |                   |
   |                                           |         |                   |
   |                                           +---------+                   |
   |                                                                         |
   +-------------------------------------------------------------------------+


+------------------------+     +------------------------+    +------------------------+
|      Player Bot        |     |      Game Wrapper      |    |         Game           |
+------------------------+     +------------------------+    +------------------------+
| onEnter()              |     | new(State)             |    |                        |
| onMessage(input)       |     | state():State          |    |                        |
| about()                |     | isGameOver():boolean   |    |                        |
| joke()                 |     | send(String):String    |    |                        |
| help()                 |     | intro(): String        |    |                        |
| quit()                 +---->+                        +--->+                        |
| format(String):String  |     |                        |    |                        |
| startGame(String)      |     |                        |    |                        |
| listGames()            |     |                        |    |                        |
| debug()                |     |                        |    |                        |
| factoryReset()         |     |                        |    |                        |
|                        |     |                        |    |                        |
|                        |     |                        |    |                        |
+------------------------+     +------------------------+    +------------------------+

Diagram powered by http://asciiflow.com/
