# Player Bot

Hangouts chatbot specialized to integrate with games.

## Development instructions

### To play on a browser (using index.html)
`gulp serve`

### To run automated tests
`node test.js`

### To generate Google Chat installs
`gulp google`

## Release Notes

### 1.0
- ✅Basic infrastructure on Google Cloud
   - ✅Project
   - ✅API configuration
   - ✅Icon
   - ✅Script
- ✅Missionaries And Cannibals game (JS)
- ✅Missionaries And Cannibals integration to bot
- ✅Scripts and template for reports based on AWS Cloud Watch
- ✅Bot commands:
   - ✅help
   - ✅list
   - ✅quit
   - ✅about
   - ✅feedback
   - ✅play
- ✅Feedbacks from play test
   - ✅Make commands shorter and simpler

### 2.0
- HTML page to simulate chat
   - ✅Gulp to serve the page
   - ✅Page to integrate with Player Bot (and MC)
   - ✅Support local storage (browser)
   - ✅UI optimization
   - ✅Support hyperlinks <https://bitbucket.org/marce1o/|Marcelo Augusto>
   - ✅Support monospace characters `monospace`
- ✅ Incorporate second game: Switch
   - ✅ List game
   - ✅ Start game
   - ✅ Interact with game
   - ✅ Quit game
   - ✅ Update Switch with potential updates
   - ✅ Update Missionaries And Cannibals with potential updates
- ✅ Debug command
- ✅ Factory reset command
- ✅ Standardize game object instantiation
- ✅ Standardize game play
- ✅ Fix shortcuts to choose a game
- ✅ Review unit testing suite
- Build to Google Chat
   - Create gulp task
   - Create Google Chat wrapper for Player Bot
- Add analytics capability

### 3.0
- Externalize JOKES
- Add a third game
- Highlight new games
- Create framework to ease new game addition
- Add "last command" to web interface
    - Use the "up" arrow
    - Bring the last command used to the input box
- Add "quick commands" to web interface
    - Use the "down" arrow
    - Display navigable buttons for "help", "Yes", "No", "Last command"
- Implement feedback from BETA
    - Implement "Play again"
    - Implement "Thanks, thank you"
    - Implement "Nice, cool"
    - Implement "Hi, hello, wasup"
    - Implement choice of game with game name (e.g. "Missionaries")
