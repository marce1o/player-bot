var fs = require('fs');

var aggregated = {};

fs.renameSync('./report.csv', './report.csv.old ');

fs.readdir('./logs', (err, files) => {
  if (err) {
    console.error(err);
  }
  for (file of files) {
    console.log('Processing file ', file);
    var data = fs.readFileSync('./logs/' + file)
    var entries = JSON.parse(data);
    for (entry of entries) {
      var processID = entry.labels['script.googleapis.com/process_id'];

      var message = entry.textPayload;
      var timestamp = entry.timestamp;

      if (!aggregated[processID]) {
        aggregated[processID] = {};
      }
      aggregated[processID].timestamp = entry.receiveTimestamp;
      aggregated[processID][timestamp] = message;
    }
  }

  // console.log(aggregated);

  var rows = [];

  for (var processID in aggregated) {
    if (aggregated.hasOwnProperty(processID)) {
      var row = {};
      var request = aggregated[processID]
      row.timestamp = request.timestamp
      for (var timestamp in request) {
        if (request.hasOwnProperty(timestamp)) {
          var message = request[timestamp];
          if (message && message !== '') {

            var user = '';

            var match = message.match(/"user"(.)+"displayName":"([^"]+)"/);
            if (match) {
              user = match[2];
            }
            row.user = user;

            if (message.startsWith('bot: ')) {
              row.robot = message.substr(5);
            } else if (message.startsWith('user: ')) {
              row.player = message.substr(6);
            }
          }
        }
      }
      rows.push(row);
    }
  }


  for (row of rows) {
    fs.appendFileSync(
      './report.csv',
      '"' + row.timestamp + '","' + row.user + '","' + row.player + '","' + row.robot + '"\n'
    );
  }

});
