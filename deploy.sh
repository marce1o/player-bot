rm -rf dist/*

cp ../missionaries-and-cannibals/mc.js .

cat robot.js \
| wc -l \
| \xargs -I%lines% bash -c 'echo $((%lines%-15))' \
| xargs -I%lines% head -n %lines% robot.js > dist/Code.gs

cat dist/Code.gs | pbcopy
