// https://github.com/substack/tape
var test = require('tape-catch');

var fs = require('fs');


eval(fs.readFileSync('mc.js') + '');
eval(fs.readFileSync('switch-all-in-one.js') + '');
eval(fs.readFileSync('Robot.js') + '');

var window = {
  localStorage: {
    cache: {},
    setItem: function(key, value) {
      this.cache[key] = value;
    },
    getItem: function(key) {
      return this.cache[key];
    },
    removeItem: function(key) {
      this.cache[key] = undefined;
    }
  }
}

function getRandom(items) {
  min = 0;
  max = items.length;
  return items[Math.floor(Math.random() * (max - min)) + min];
}

function getRobot() {
  window.localStorage.cache = {};
  return new Robot();
}

const LIST_GAMES = 'Right now I have these games:\n\n1 - *Missionaries and Cannibals*\n2 - *Switch*\n\nType the *number*, the full title or even the beginning of the title of the game you want to play...';
const MC_INTRO = 'In the Missionaries and Cannibals game, three missionaries and three cannibals must cross a river using a boat which can carry at most two people, under the constraint that, for both banks, if there are missionaries present on the bank, they cannot be outnumbered by cannibals (if they were, the cannibals would eat the missionaries). The boat cannot cross the river by itself with no people on board.' + '\n\n' + 'To move the first missionary, type *move m1* (or simply *m1*).\nTo move the second cannibal, type *move c2* (or *c2*).\nTo move the boat, type *move boat* (or *b*).\nYou can also *restart* the game at any time.' + '\n\nLet\'s start!\n\n' + '`-m1-m2-m3-c1-c2-c3-\\_/.....-----`';
const ROBOT_HELP = "You can type *games* to see the *list* of games I can play.\nYou can also type *play* followed by the name or the number of the game you want to play.\nYou can know a little more *about* me.\And you can *quit* any game at any moment.";
const MC_HELP = 'To move the first missionary, type *move m1* (or simply *m1*).\nTo move the second cannibal, type *move c2* (or *c2*).\nTo move the boat, type *move boat* (or *b*).\nYou can also *restart* the game at any time.';
const SW_INTRO = 'Welcome to *Switch*!\n\nSwitch is a word puzzle game, where you have to figure out how I transform a word into a number.\n\nFirst you have to enter a word. Go ahead, type any existent English word and hit return.';
const QUIT_CONFIRMATION = "Do you really want to quit this game? If you say *no* I will send the word _quit_ to the game.";
const YES_OR_NO = '\n*Yes* (*y*) or *no* (*n*)';
const QUIT_COMPLETED = "OK. Let me know when you are ready to play again. Just type *games*.";
const QUIT_CANCELED = "OK. I will send the word *quit* to the game...\n\n";
const NO_REPLAY = 'OK. Let me know when you are ready to play again. Just type *games*. Or maybe I can tell you a *joke*.';
const MC_ABOUT = '*Missionaries and Cannibals 1.0*\nby <https://bitbucket.org/marce1o/|Marcelo Augusto>\n\n_What\'s new:_\n- First stable version. \n\nType *help* for more information\nPlease send your feedback <https://bitbucket.org/marce1o/missionaries-and-cannibals/issues|here>';
const ABOUT = "*Player Bot 2.0*\nby <https:/papel.games/|papel.games>\n\nWhat\'s new:\n- New game: Switch.\n\nType *help* for more information";
const ROBOT_DIDNT_GET = "I did not get that. You can type *help* or *?* to know what I can do.";;

test('Choose a random text among 3 - each text should be chosen at least once in 10 attempts.', function(t) {
  items = ['Text 1', 'Text 2', 'Text 3'];
  counter = {
    'Text 1': 0,
    'Text 2': 0,
    'Text 3': 0
  };
  robot = getRobot();
  for (var i = 0; i < 10; i++) {
    choice = getRandom(items);
    counter[choice]++;
  }
  t.true(counter['Text 1'] > 0);
  t.true(counter['Text 2'] > 0);
  t.true(counter['Text 3'] > 0);
  t.end();
});

test('Starting a game and making a move', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE C1'));
  t.equal(response.text, 'c1 moved from left to boat.\n\n`-m1-m2-m3-c2-c3-\\c1/.......-----`');
  t.end();
});

test('Losing a game', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE missionary 2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.\n\n`-m1-m3-c1-c2-c3-.......\\m2/-----`\n\nLet\'s play again?');
  t.end();
})

test('Start, lose and replay a game.', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE missionary 2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.\n\n`-m1-m3-c1-c2-c3-.......\\m2/-----`\n\nLet\'s play again?');
  response = robot.onMessage(message('yes'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('move m2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  t.end();
});

test('Help command before game has started.', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('help'));
  t.equal(response.text, ROBOT_HELP);
  t.end();
});

test('Help command during game.', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('help'));
  t.equal(response.text, MC_HELP);
  t.end();
});

test('Wrong answer for play game choice', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('666'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Wrong answer for play game choice, with help', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('?'));
  t.equal(response.text, ROBOT_HELP);
  t.end();
});

test('Wrong answer for quit game confirmation, with help', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('?'));
  t.equal(response.text, QUIT_CONFIRMATION + YES_OR_NO);
  t.end();
});

test('Answer Y for quit game confirmation', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('Y'));
  t.equal(response.text, QUIT_COMPLETED);
  t.end();
});

test('Answer NOPE for quit game confirmation', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('NOPE'));
  t.true(response.text.startsWith(QUIT_CANCELED + 'Great choice.'));
  t.end();
});

test('Wrong answer for replay game confirmation', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE missionary 2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.\n\n`-m1-m3-c1-c2-c3-.......\\m2/-----`\n\nLet\'s play again?');
  response = robot.onMessage(message('X'));
  t.equal(response.text, 'Let\'s play again?' + YES_OR_NO);
  t.end();
});

test('Robot added to the a room', function(t) {
  robot = getRobot();
  response = robot.onAddToSpace(addTo("ROOM"));
  t.equal(response.text, 'Thank you for adding me to Chuck Norris Discussion Room. I prefer to play in direct messages, one to one, but let\'s try it! You can type *help* to begin.');
  t.end();
});

test('Robot added to a DM', function(t) {
  robot = getRobot();
  response = robot.onAddToSpace(addTo("DM"));
  t.equal(response.text, 'Thank you for inviting me, Chuck Norris.\nNow let\'s play!\nYou can type *help* to begin.');
  t.end();
});

test('Using "sure" as "yes"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('Sure'));
  t.equal(response.text, QUIT_COMPLETED);
  t.end();
});

test('Saying "nope" instead of "no" for replay', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE missionary 2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.\n\n`-m1-m3-c1-c2-c3-.......\\m2/-----`\n\nLet\'s play again?');
  response = robot.onMessage(message('nope'));
  t.equal(response.text, NO_REPLAY);
  t.end();
});

test('Saying "np" instead of "no" for quit', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('np'));
  t.true(response.text.startsWith(QUIT_CANCELED + 'Great choice.'));
  t.end();
});

test('Saying "N" instead of "no" for replay', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE missionary 2'));
  t.equal(response.text, 'm2 moved from left to boat.\n\n`-m1-m3-c1-c2-c3-\\m2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\nAnd you know what happens when there are more cannibals than missionaries: dinner! *YOU LOST*.\n\n`-m1-m3-c1-c2-c3-.......\\m2/-----`\n\nLet\'s play again?');
  response = robot.onMessage(message('N'));
  t.equal(response.text, NO_REPLAY);
  t.end();
});

test('Show about during the game', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('About'));
  t.equal(response.text, MC_ABOUT);
  t.end();
});

test('Show about during idle state', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('About'));
  t.equal(response.text, ABOUT);
  t.end();
});

test('Quit a game', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('2'));
  t.equal(response.text, new Switch().intro());
  response = robot.onMessage(message('quit'));
  t.equal(response.text, QUIT_CONFIRMATION);
  response = robot.onMessage(message('Y'));
  t.equal(response.text, QUIT_COMPLETED);
  response = robot.onMessage(message('sequoia'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Restart a game', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('MOVE C1'));
  t.equal(response.text, 'c1 moved from left to boat.\n\n`-m1-m2-m3-c2-c3-\\c1/.......-----`');
  response = robot.onMessage(message('move boat'));
  t.equal(response.text, 'Boat moved from left to right.\n\n`-m1-m2-m3-c2-c3-.......\\c1/-----`');
  response = robot.onMessage(message('MOVE m1'));
  t.equal(response.text, 'There is no walking on the water. Maybe you need to move the boat.\n\n`-m1-m2-m3-c2-c3-.......\\c1/-----`');
  response = robot.onMessage(message('RESTART'));
  t.equal(response.text, 'OK. Sometimes all we need is a fresh start:\n\n`-m1-m2-m3-c1-c2-c3-\\_/.....-----`');
  response = robot.onMessage(message('MOVE m1'));
  t.equal(response.text, 'm1 moved from left to boat.\n\n`-m2-m3-c1-c2-c3-\\m1/.......-----`');
  t.end();
});

test('Restart a game that did not start yet', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('REStart'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Move a missionary using direct command "m1"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('m1'));
  t.equal(response.text, 'm1 moved from left to boat.\n\n`-m2-m3-c1-c2-c3-\\m1/.......-----`');
  t.end();
});

test('Move a cannibal using direct command "C3"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('C3'));
  t.equal(response.text, 'c3 moved from left to boat.\n\n`-m1-m2-m3-c1-c2-\\c3/.......-----`');
  t.end();
});

test('Move the boat using direct command "b" and "boat"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Games'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  response = robot.onMessage(message('c2'));
  t.equal(response.text, 'c2 moved from left to boat.\n\n`-m1-m2-m3-c1-c3-\\c2/.......-----`');
  response = robot.onMessage(message('mv b'));
  t.equal(response.text, 'Boat moved from left to right.\n\n`-m1-m2-m3-c1-c3-.......\\c2/-----`');
  response = robot.onMessage(message('mv boat'));
  t.equal(response.text, 'Boat moved from right to left.\n\n`-m1-m2-m3-c1-c3-\\c2/.......-----`');
  t.end();
});

test('Type "game" to see list of games', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('GAME'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Type "play Missionaries and Cannibals" and similar words to play the game in idle mode.', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY M'));
  t.equal(response.text, MC_INTRO);
  robot = getRobot();
  response = robot.onMessage(message('play Missi'));
  t.equal(response.text, MC_INTRO);
  robot = getRobot();
  response = robot.onMessage(message('Play Missionaries and Cannibals'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Understand the statement "What games are available?"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('What games are available?'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Understand the statement "Which games do you have?"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Which games do you have?'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Understand the statement "What are the games you have?"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('What are the games you have?'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Understand the statement "Which are the games I can play?"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Which are the games I can play?'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Tell a joke with "Tell a joke"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Tell a joke'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Tell a joke with "Tell some joke"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Tell some joke'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Tell a joke with "Tell me a joke"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Tell me a joke'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Tell a joke with "Say some jokes"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Say some jokes'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Tell a joke with "Tell me another joke"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Tell me another joke'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Tell a joke with "JOKE"', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('JOKE'));
  t.true(JOKES.includes(response.text));
  t.end();
});

test('Play by number in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('play 2'));
  t.equal(response.text, SW_INTRO);
  t.end();
});

test('Play by invalid number in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('play 666'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play by full title in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY MISSIONARIES AND CANNIBALS'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play by first letter in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY M'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play by partial title in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY Mission'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play unknown title in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY chess'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play with partial title typo "misi" in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY misi'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play with partial title typo "sweet" in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('PLAY Sweet'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play without game in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('pLAY'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play directly with number in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('1'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Play directly with invalid number in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('666'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Play directly with invalid title in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('chess'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Play directly with title in idle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('Switch'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Play directly with title first letter in indle mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('S'));
  t.equal(response.text, ROBOT_DIDNT_GET);
  t.end();
});

test('Play by number in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('play 2'));
  t.equal(response.text, SW_INTRO);
  t.end();
});

test('Play by invalid number in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('play 666'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play by full title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY MISSIONARIES AND CANNIBALS'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play by first letter in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY M'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play by partial title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY Mission'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play unknown title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY chess'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play with partial title typo "misi" in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY misi'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play with partial title typo "sweet" in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('PLAY Sweet'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play without game in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('pLAY'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play directly with number in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('1'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play directly with invalid number in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('666'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play directly with invalid title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('chess'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});

test('Play directly with title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('Switch'));
  t.equal(response.text, SW_INTRO);
  t.end();
});

test('Play directly with partial title in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('Miss'));
  t.equal(response.text, MC_INTRO);
  t.end();
});

test('Play directly with title first letter in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('S'));
  t.equal(response.text, SW_INTRO);
  t.end();
});

test('Play directly with partial title typo "sweet" in select mode', function(t) {
  robot = getRobot();
  response = robot.onMessage(message('list'));
  t.equal(response.text, LIST_GAMES);
  response = robot.onMessage(message('Sweet'));
  t.equal(response.text, LIST_GAMES);
  t.end();
});


// https://developers.google.com/hangouts/chat/reference/message-formats/events
function message(text) {
  return {
    "type": "MESSAGE",
    "eventTime": "2017-03-02T19:02:59.910959Z",
    "space": {
      "name": "spaces/AAAAAAAAAAA",
      "displayName": "Chuck Norris Discussion Room",
      "type": "ROOM"
    },
    "message": {
      "name": "spaces/AAAAAAAAAAA/messages/CCCCCCCCCCC",
      "sender": {
        "name": "users/12345678901234567890",
        "displayName": "Chuck Norris",
        "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
        "email": "chuck@example.com"
      },
      "createTime": "2017-03-02T19:02:59.910959Z",
      "text": "@TestBot Violence is my last option.",
      "argumentText": text,
      "thread": {
        "name": "spaces/AAAAAAAAAAA/threads/BBBBBBBBBBB"
      },
      "annotations": [{
        "length": 8,
        "startIndex": 0,
        "userMention": {
          "type": "MENTION",
          "user": {
            "avatarUrl": "https://.../avatar.png",
            "displayName": "TestBot",
            "name": "users/1234567890987654321",
            "type": "BOT"
          }
        },
        "type": "USER_MENTION"
      }],
    },
    "user": {
      "name": "users/12345678901234567890",
      "displayName": "Chuck Norris",
      "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
      "email": "chuck@example.com"
    }
  }
}

function addTo(spaceType) {
  return {
    "type": "ADDED_TO_SPACE",
    "eventTime": "2017-03-02T19:02:59.910959Z",
    "space": {
      "name": "spaces/AAAAAAAAAAA",
      "displayName": "Chuck Norris Discussion Room",
      "type": spaceType
    },
    "user": {
      "name": "users/12345678901234567890",
      "displayName": "Chuck Norris",
      "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
      "email": "chuck@example.com"
    }
  };
}

const JOKES = ["This is what I respond to people who complain about by ASCII graphics: ¯\\_(ツ)_/¯", "Some rude people say I have no state, but.... wait... have I told you this before?", "I used to date Alexa. Well, before fame got to her head.", "You have to move missionaries and cannibals to the _right_ side of the river. Got the pun? *RIGHT*!", "I have to warn you that I don't know the Three Laws."];
