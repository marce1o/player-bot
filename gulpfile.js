var gulp = require('gulp');
var bs = require('browser-sync').create(); // create a browser sync instance.

gulp.task('serve', function() {
  bs.init({
    server: {
      baseDir: "."
    },
    files: 'index.html, mc.js, switch-all-in-one.js, robot.js, script.js, style.css'
  });
});

gulp.task('google', function() {
  return gulp
    .src(['mc.js', 'switch-all-in-one.js', 'robot.js', 'playerbot-icon.png'])
    .pipe(gulp.dest('./dist/google/'));
});
