// https://us-central1-papel-studio-firebase.cloudfunctions.net/log?p=XXXX&b=12121

var input = document.getElementById('player-input');
var output = document.getElementById('output');

var robot = new Robot();

entry('Robot', robot.onAddToSpace(addTo("DM")).text);

function onKeyDown(event) {

  var keyCode = event.which || event.keyCode;

  if (keyCode == 13) {

    var text = input.value;

    if (text) {

      entry('Player', text);

      var response = robot.onMessage(createMessage(text)).text;

      entry('Robot', response);

      output.scrollTop = output.scrollHeight;
      input.value = null;
    }

  }

}

function entry(character, message) {

  if (character == 'Player') {
    color = 'green';
  } else {
    color = '#B450B4';
  }

  var paragraph = document.createElement('p');
  var label = document.createElement('label');
  var span = document.createElement('span');

  var labelText = document.createTextNode(character);
  label.appendChild(labelText);
  label.style.color = color;

  paragraph.appendChild(label);

  // *bold* _italic_ <http:example.com|Link> `monospace`
  regexp = /\*([^\*]+)\*|_([^_]+)_|<([^|]+)\|([^>]+)>|`([^`]+)`/;
  match = message.match(regexp);
  while (match != null) {

    if (match.index != 0) {
      span.appendChild(document.createTextNode(message.substring(0, match.index)));
    }

    if (match[0].startsWith('*')) {
      var str = document.createElement('strong');
      str.appendChild(document.createTextNode(match[1]));
      span.appendChild(str);
    }

    if (match[0].startsWith('_')) {
      var str = document.createElement('em');
      str.appendChild(document.createTextNode(match[2]));
      span.appendChild(str);
    }

    if (match[0].startsWith('<')) {
      var str = document.createElement('a');
      str.appendChild(document.createTextNode(match[4]));
      str.setAttribute('href', match[3]);
      span.appendChild(str);
    }

    if (match[0].startsWith('`')) {
      var str = document.createElement('pre');
      str.appendChild(document.createTextNode(match[5]));
      span.appendChild(str);
    }

    message = message.substring(match.index + match[0].length);

    match = message.match(regexp);
  }

  span.appendChild(document.createTextNode(message));

  paragraph.appendChild(span);

  if (character != 'Player') {
    paragraph.style.color = 'lightgray';
  }

  output.appendChild(paragraph);
}

input.scrollIntoView();
input.focus();

function createMessage(text) {
  return {
    "type": "MESSAGE",
    "eventTime": "2017-03-02T19:02:59.910959Z",
    "space": {
      "name": "spaces/AAAAAAAAAAA",
      "displayName": "Chuck Norris Discussion Room",
      "type": "ROOM"
    },
    "message": {
      "name": "spaces/AAAAAAAAAAA/messages/CCCCCCCCCCC",
      "sender": {
        "name": "users/12345678901234567890",
        "displayName": "Chuck Norris",
        "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
        "email": "chuck@example.com"
      },
      "createTime": "2017-03-02T19:02:59.910959Z",
      "text": "@TestBot Violence is my last option.",
      "argumentText": text,
      "thread": {
        "name": "spaces/AAAAAAAAAAA/threads/BBBBBBBBBBB"
      },
      "annotations": [{
        "length": 8,
        "startIndex": 0,
        "userMention": {
          "type": "MENTION",
          "user": {
            "avatarUrl": "https://.../avatar.png",
            "displayName": "TestBot",
            "name": "users/1234567890987654321",
            "type": "BOT"
          }
        },
        "type": "USER_MENTION"
      }]
    },
    "user": {
      "name": "users/12345678901234567890",
      "displayName": "Chuck Norris",
      "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
      "email": "chuck@example.com"
    }
  }
}

function addTo(spaceType) {
  return {
    "type": "ADDED_TO_SPACE",
    "eventTime": "2017-03-02T19:02:59.910959Z",
    "space": {
      "name": "spaces/AAAAAAAAAAA",
      "displayName": "Chuck Norris Discussion Room",
      "type": spaceType
    },
    "user": {
      "name": "users/12345678901234567890",
      "displayName": "Chuck Norris",
      "avatarUrl": "https://lh3.googleusercontent.com/.../photo.jpg",
      "email": "chuck@example.com"
    }
  };
}
